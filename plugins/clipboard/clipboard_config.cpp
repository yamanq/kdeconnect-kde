/**
 * SPDX-FileCopyrightText: 2013 Albert Vaca <albertvaka@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "clipboard_config.h"
#include "ui_clipboard_config.h"

#include <KPluginFactory>

K_PLUGIN_FACTORY(ClipboardConfigFactory, registerPlugin<ClipboardConfig>();)

ClipboardConfig::ClipboardConfig(QWidget* parent, const QVariantList& args)
    : KdeConnectPluginKcm(parent, args, QStringLiteral("kdeconnect_clipboard_config"))
    , m_ui(new Ui::ClipboardConfigUi())
{
    m_ui->setupUi(this);

    connect(m_ui->rad_sync, SIGNAL(toggled(bool)), this, SLOT(changed()));
    connect(m_ui->rad_intent, SIGNAL(toggled(bool)), this, SLOT(changed()));
}

ClipboardConfig::~ClipboardConfig()
{
    delete m_ui;
}

void ClipboardConfig::defaults()
{
    KCModule::defaults();
    m_ui->rad_sync->setChecked(true);
    m_ui->rad_intent->setChecked(false);
    Q_EMIT changed(true);
}

void ClipboardConfig::load()
{
    KCModule::load();
    bool sync = config()->getBool(QStringLiteral("conditionSync"), true);
    m_ui->rad_sync->setChecked(sync);
    m_ui->rad_intent->setChecked(!sync);


    Q_EMIT changed(false);
}

void ClipboardConfig::save()
{
    config()->set(QStringLiteral("conditionSync"), m_ui->rad_sync->isChecked());
    KCModule::save();
    Q_EMIT changed(false);
}

#include "clipboard_config.moc"
