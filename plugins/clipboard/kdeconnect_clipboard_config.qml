/**
 * SPDX-FileCopyrightText: 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1
import org.kde.kirigami 2.5 as Kirigami
import org.kde.kdeconnect 1.0

Kirigami.FormLayout {

    property string device

    KdeConnectPluginConfig {
        id: config
        deviceId: device
        pluginName: "kdeconnect_clipboard"
    }

    Component.onCompleted: {
        sync.checked = config.get("conditionSync", true)
    }

    RadioButton {
        id: sync
        onCheckedChanged: config.set("conditionSync", checked)
        text: i18n("On every change")
    }

    RadioButton {
        text: i18n("Only on intent")
        checked: !sync.checked
    }

}
